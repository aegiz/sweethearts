This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available commands

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>

## Considerations regarding the task

In this technical test I paid a special attention to break down the app in small reusable components.

To go faster I used plain CSS (and BEM methodology), an alternative could have been to use styled components.

Demo URL: https://heuristic-engelbart-3d3ea4.netlify.com/

## Deployment

As requested, I included in this repo minified version of the code (in the build folder).

You can easily deploy this code with a tool like Netlify using these commands:

```sh
$ npm install netlify-cli -g
$ netlify deploy
```
