import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Homepage from "./routes/Homepage/Homepage";
import Result from "./routes/Result/Result";
import NotFound from "./routes/NotFound";

function App() {
	return (
		<BrowserRouter>
			<Switch>
				<Route exact path="/" component={Homepage} />
				<Route path="/result" component={Result} />
				<Route component={NotFound} />
			</Switch>
		</BrowserRouter>
	);
}

export default App;
