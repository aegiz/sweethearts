import React from "react";
import "./Form.css";
import PropTypes from "prop-types";
import Slider from "../Slider/Slider";
import MultiSelect from "../MultiSelect/MultiSelect";
import MultiRadio from "../MultiRadio/MultiRadio";
import CTA from "../CTA/CTA";

export default class Form extends React.Component {
	static propTypes = {
		history: PropTypes.object,
		updateInputs: PropTypes.func,
		inputs: PropTypes.shape({
			age: PropTypes.number,
			height: PropTypes.number
		})
	};

	goToResults = event => {
		event.preventDefault();
		this.props.history.push({
			pathname: "/result",
			state: [this.props.inputs]
		});
	};

	render() {
		return (
			<form name="male" onSubmit={this.goToResults}>
				<div className="row">
					<h4>Age</h4>
					<Slider
						sliderType="age"
						updateInputs={this.props.updateInputs}
						defaultValue={this.props.inputs.age}
						minValue={14}
						maxValue={100}
					/>
				</div>
				<div className="row">
					<h4>Height (cm)</h4>
					<Slider
						sliderType="height"
						updateInputs={this.props.updateInputs}
						defaultValue={this.props.inputs.height}
						minValue={140}
						maxValue={220}
					/>
				</div>
				<div className="row">
					<h4>Haircolor</h4>
					<MultiSelect updateInputs={this.props.updateInputs} />
				</div>
				<div className="row">
					<h4>Hairlength</h4>
					<MultiRadio
						radioChoices={{
							long: 1,
							middle: 2,
							short: 3,
							bald: 0
						}}
						radioType="hair-length"
						defaultValue="long"
						updateInputs={this.props.updateInputs}
					/>
				</div>
				<div className="row">
					<h4>Eyecolor</h4>
					<MultiRadio
						radioChoices={{
							blue: 3,
							green: 2,
							brown: 0,
							grey: 1
						}}
						radioType="eye-color"
						defaultValue="blue"
						updateInputs={this.props.updateInputs}
					/>
				</div>
				<div className="row">
					<h4>Beard</h4>
					<MultiRadio
						radioChoices={{
							none: 0,
							small: 1,
							middle: 2,
							full: 3
						}}
						radioType="beard"
						defaultValue="none"
						updateInputs={this.props.updateInputs}
					/>
				</div>
				<div className="row">
					<h4>Body</h4>
					<MultiRadio
						radioChoices={{
							muscle: 2,
							normal: 1,
							over: 0
						}}
						radioType="body"
						defaultValue="normal"
						updateInputs={this.props.updateInputs}
					/>
				</div>
				<div className="row">
					<CTA
						text="Calculate"
						type="submit"
						buttonSize="large"
						buttonColor="orange"
					/>
				</div>
			</form>
		);
	}
}
