import React from "react";
import RangeSlider from "react-rangeslider";
import "react-rangeslider/lib/index.css";
import PropTypes from "prop-types";
import "./Slider.css";

export default class Slider extends React.Component {
	static propTypes = {
		sliderType: PropTypes.string,
		updateInputs: PropTypes.func,
		defaultValue: PropTypes.number,
		minValue: PropTypes.number,
		maxValue: PropTypes.number
	};

	state = {
		value: this.props.defaultValue
	};

	handleChange = value => {
		this.setState({ value });
	};

	handleChangeComplete = () => {
		this.props.updateInputs(this.props.sliderType, this.state.value);
	};

	render() {
		const DEFAULT = {
			MIN: 0,
			MAX: 100
		};
		const { value } = this.state;
		return (
			<div className="slider-container">
				<div className="value">{value}</div>
				<RangeSlider
					min={this.props.minValue || DEFAULT.MIN}
					max={this.props.maxValue || DEFAULT.MAX}
					value={value}
					onChange={this.handleChange}
					onChangeComplete={this.handleChangeComplete}
				/>
			</div>
		);
	}
}
