import React from "react";
import "./MultiRadio.css";
import PropTypes from "prop-types";
import { switchcase } from "../../helpers";

export default class MultiRadio extends React.Component {
	static propTypes = {
		radioChoices: PropTypes.object,
		radioType: PropTypes.string,
		defaultValue: PropTypes.string,
		updateInputs: PropTypes.func
	};

	state = {
		selected: this.props.radioType + "-" + this.props.defaultValue
	};

	handleChange = event => {
		this.setState({ selected: event.target.id });
		this.props.updateInputs(
			this.props.radioType,
			switchcase(this.props.radioChoices, event.target.value, 0)
		);
	};

	render() {
		return (
			<div className={`MultiRadio MultiRadio--${this.props.radioType}`}>
				<nav>
					{Object.keys(this.props.radioChoices).map((choice, i) => (
						<React.Fragment key={i}>
							<input
								type="radio"
								name={this.props.radioType}
								value={choice}
								id={this.props.radioType + "-" + choice}
								onChange={this.handleChange}
								checked={
									this.state.selected ===
									this.props.radioType + "-" + choice
								}
							/>
							<label htmlFor={this.props.radioType + "-" + choice}>
								{choice}
							</label>
						</React.Fragment>
					))}
				</nav>
			</div>
		);
	}
}
