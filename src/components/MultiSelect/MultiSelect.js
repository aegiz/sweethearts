import React from "react";
import "./MultiSelect.css";
import PropTypes from "prop-types";
import { switchcase } from "../../helpers";

export default class MultiSelect extends React.Component {
	static propTypes = {
		updateInputs: PropTypes.func
	};

	handleChange = event => {
		this.props.updateInputs(
			"hair-color",
			switchcase(
				{
					blonde: 2,
					brown: 1,
					black: 1,
					red: 0,
					grey: 0
				},
				event.target.value,
				0
			)
		);
	};
	render() {
		return (
			<div className="MultiSelect">
				<select
					name="haircolor"
					className="u-full-width"
					onChange={this.handleChange}
				>
					<option value="blonde">blonde</option>
					<option value="brown">brown</option>
					<option value="black">black</option>
					<option value="red">red</option>
					<option value="grey">grey</option>
				</select>
			</div>
		);
	}
}
