import React from "react";
import "./Header.css";

export default class Header extends React.Component {
	render() {
		return (
			<header className="Header">
				<h1>How many camels for your boyfriend?</h1>
			</header>
		);
	}
}
