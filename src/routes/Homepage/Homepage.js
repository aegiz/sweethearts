import React from "react";
import "./Homepage.css";
import PropTypes from "prop-types";
import Header from "../../components/Header/Header";
import Form from "../../components/Form/Form";

class Homepage extends React.Component {
	static propTypes = {
		history: PropTypes.object
	};
	state = {
		inputs: {
			age: 22,
			height: 176,
			"hair-color": 2,
			"hair-length": 1,
			"eye-color": 3,
			beard: 0,
			body: 1
		}
	};
	updateInputs = (key, updatedInput) => {
		// Prevent mutating this.state using Object Spread
		const inputs = { ...this.state.inputs };
		inputs[key] = updatedInput;
		this.setState({ inputs });
	};
	render() {
		return (
			<div className="Homepage">
				<Header />
				<Form
					history={this.props.history}
					inputs={this.state.inputs}
					updateInputs={this.updateInputs}
				/>
			</div>
		);
	}
}

export default Homepage;
