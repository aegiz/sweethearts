import React from "react";
import "./Result.css";
import PropTypes from "prop-types";
import Header from "../../components/Header/Header";
import CTA from "../../components/CTA/CTA";

export default class Result extends React.Component {
	static propTypes = {
		location: PropTypes.object,
		history: PropTypes.object
	};

	openFacebookPopup = () => {
		window.open(
			"https://www.facebook.com/sharer.php?u=http://kamelrechner.de/",
			"_blank",
			"width=400,height=500"
		);
	};

	goToHome = () => {
		this.props.history.push({
			pathname: "/"
		});
	};

	camelAgeRelationship = age => {
		let camel = 0;
		if (age < 18) {
			camel = 1;
		} else if (age < 36) {
			camel = 3;
		} else if (age < 51) {
			camel = 2;
		} else {
			camel = 0;
		}
		return camel;
	};

	camelHeightRelationship = height => {
		let camel = 0;
		if (height < 169) {
			camel = 0;
		} else if (height < 190) {
			camel = 2;
		} else if (height < 220) {
			camel = 1;
		} else {
			camel = 0;
		}
		return camel;
	};

	calculateCamels = inputs => {
		return (
			this.camelAgeRelationship(inputs[0].age) * 8 +
			this.camelHeightRelationship(inputs[0].height) * 6 +
			inputs[0]["hair-color"] * 5 +
			inputs[0]["hair-length"] * 6 +
			inputs[0]["eye-color"] * 6
		);
	};

	render() {
		const camels = this.calculateCamels(this.props.location.state);
		return (
			<div className="Result">
				<Header />
				<p>Your boyfriend is worth</p>
				<p className="result-camel"> {camels}</p>
				<p>camels.</p>
				<CTA
					text="Post on facebook"
					type="button"
					onClick={this.openFacebookPopup}
					buttonSize="medium"
					buttonColor="blue"
				/>
				<CTA
					text="Calculate again"
					type="button"
					onClick={this.goToHome}
					buttonSize="large"
					buttonColor="orange"
				/>
			</div>
		);
	}
}
